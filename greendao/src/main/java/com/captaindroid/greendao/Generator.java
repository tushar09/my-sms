package com.captaindroid.greendao;

import org.greenrobot.greendao.generator.DaoGenerator;
import org.greenrobot.greendao.generator.Entity;
import org.greenrobot.greendao.generator.Schema;

public class Generator {
    public static void main(String [] args){
        Schema schema = new Schema(1, "club.tushar.mysms.db"); // Your app package name and the (.db) is the folder where the DAO files will be generated into.
        schema.enableKeepSectionsByDefault();

        addTables(schema);

        try {
            new DaoGenerator().generateAll(schema,"../app/src/main/java");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void addTables(Schema schema) {
        addSpammerEntities(schema);
        addSpamBoxEntities(schema);
    }

    private static Entity addSpamBoxEntities(Schema schema) {
        Entity spamBox = schema.addEntity("SpamBox");
        spamBox.addIdProperty().autoincrement();
        spamBox.addLongProperty("spammer_id");
        spamBox.addStringProperty("body");
        spamBox.addLongProperty("date");
        spamBox.addLongProperty("created_at");
        spamBox.addLongProperty("updated_at");
        return spamBox;
    }

    private static Entity addSpammerEntities(Schema schema) {
        Entity spammer = schema.addEntity("Spammer");
        spammer.addIdProperty().autoincrement();
        spammer.addStringProperty("number").unique().notNull();
        spammer.addStringProperty("name");
        spammer.addStringProperty("image");
        spammer.addLongProperty("created_at");
        spammer.addLongProperty("updated_at");
        return spammer;
    }
}
