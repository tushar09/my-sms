package club.tushar.mysms.eventBus;

public class OnMessage {
    private String senderNumber;
    private String messageBody;
    private int readStatus;
    private long time;

    public OnMessage(String senderNumber, String messageBody, int readStatus, long time) {
        this.senderNumber = senderNumber;
        this.messageBody = messageBody;
        this.readStatus = readStatus;
        this.time = time;
    }

    public String getSenderNumber() {
        return senderNumber;
    }

    public void setSenderNumber(String senderNumber) {
        this.senderNumber = senderNumber;
    }

    public String getMessageBody() {
        return messageBody;
    }

    public void setMessageBody(String messageBody) {
        this.messageBody = messageBody;
    }

    public int getReadStatus() {
        return readStatus;
    }

    public void setReadStatus(int readStatus) {
        this.readStatus = readStatus;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }
}
