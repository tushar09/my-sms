package club.tushar.mysms.broadcast;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.media.AudioAttributes;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.provider.Telephony;
import android.telephony.SmsMessage;
import android.util.Log;

import androidx.core.app.NotificationCompat;
import androidx.core.content.ContextCompat;

import org.greenrobot.eventbus.EventBus;

import club.tushar.mysms.R;
import club.tushar.mysms.activity.MessageDetailsActivity;
import club.tushar.mysms.db.SpamBox;
import club.tushar.mysms.db.Spammer;
import club.tushar.mysms.eventBus.OnMessage;
import club.tushar.mysms.utils.Constants;

public class SmsReceiver extends BroadcastReceiver {

    private static final String CHANNEL_ID = "mysms";

    @Override
    public void onReceive(Context context, Intent intent) {
        //Log.e("sms", "mms");
        // Retrieves a map of extended data from the intent.
        final Bundle bundle = intent.getExtras();

        try {

            if (bundle != null) {

                final Object[] pdusObj = (Object[]) bundle.get("pdus");

                for (int i = 0; i < pdusObj.length; i++) {

                    SmsMessage currentMessage = SmsMessage.createFromPdu((byte[]) pdusObj[i]);
                    String senderNum = currentMessage.getDisplayOriginatingAddress();
                    String message = currentMessage.getDisplayMessageBody();

                    Spammer spammer = Constants.getDbHelper(context).getSpammerByNumber(senderNum);
                    if(spammer == null){
                        saveSms(context, senderNum, message, 0, System.currentTimeMillis(), "inbox");
                    }else {
                        SpamBox spamBox = new SpamBox();
                        spamBox.setBody(message);
                        spamBox.setDate(System.currentTimeMillis());
                        spamBox.setCreated_at(System.currentTimeMillis());
                        spamBox.setUpdated_at(System.currentTimeMillis());
                        spamBox.setSpammer_id(spammer.getId());
                        Constants.getDbHelper(context).addToSmapBox(spamBox);
                        showBlockedSmsNotification(context, message, senderNum);
                        break;
                    }
                } // end for loop
            } // bundle is null

        } catch (Exception e) {
            Log.e("SmsReceiver", "Exception smsReceiver" +e);

        }
    }

    public boolean saveSms(Context context, String phoneNumber, String message, int readState, long time, String folderName) {
        boolean ret = false;
        try {
            //Telephony.Sms.MESSAGE_TYPE_DRAFT
            ContentValues values = new ContentValues();
            values.put(Telephony.Sms.ADDRESS, phoneNumber);
            values.put(Telephony.Sms.BODY, message);
            values.put(Telephony.Sms.READ, readState); //"0" for have not read sms and "1" for have read sms
            values.put(Telephony.Sms.DATE, time);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                Uri uri = Telephony.Sms.Sent.CONTENT_URI;
                if(folderName.equals("inbox")){
                    uri = Telephony.Sms.Inbox.CONTENT_URI;
                }
                context.getContentResolver().insert(uri, values);
            }
            else {
                context.getContentResolver().insert(Uri.parse("content://sms/" + folderName), values);
            }

            ret = true;
            OnMessage om = new OnMessage(phoneNumber, message, readState, time);
            EventBus.getDefault().post(om);
            showNotification(context, message, phoneNumber);
        } catch (Exception ex) {
            ex.printStackTrace();
            ret = false;
        }
        return ret;
    }

    public String getContactName(Context context, final String phoneNumber) {
        Uri uri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(phoneNumber));

        String[] projection = new String[]{ContactsContract.PhoneLookup.DISPLAY_NAME};

        String contactName = phoneNumber;
        Cursor cursor = context.getContentResolver().query(uri, projection, null, null, null);

        if (cursor != null) {
            if (cursor.moveToFirst()) {
                contactName = cursor.getString(0);
            }
            cursor.close();
        }
        return contactName;
    }

    private void showNotification(Context context, String msg, String address) {
        Intent notificationIntent;
        String name = getContactName(context, address);
        notificationIntent = new Intent(context, MessageDetailsActivity.class);

        notificationIntent.putExtra("header", address);
        notificationIntent.putExtra("body", msg);
        notificationIntent.putExtra("reply", true);
        notificationIntent.putExtra("name", name);
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

        PendingIntent contentIntent = PendingIntent.getActivity(context, (int)System.currentTimeMillis() / 3600, notificationIntent,
                PendingIntent.FLAG_UPDATE_CURRENT);


        NotificationCompat.Builder builder =
                new NotificationCompat.Builder(context)
                        .setContentTitle(name)
                        .setContentText(msg)
                        .setSmallIcon(R.drawable.ic_stat_name)
                        .setAutoCancel(true)
                        .setStyle(new NotificationCompat.BigTextStyle())
                        .setColor(ContextCompat.getColor(context, R.color.colorPrimaryDark))
                        .setChannelId(CHANNEL_ID)
                        .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                        .setContentIntent(contentIntent)
                        //.setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                        .setDefaults(Notification.DEFAULT_LIGHTS | Notification.DEFAULT_VIBRATE)
                        .setPriority(NotificationManager.IMPORTANCE_HIGH);
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            AudioAttributes audioAttributes = new AudioAttributes.Builder()
                    .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
                    .setUsage(AudioAttributes.USAGE_ALARM)
                    .build();
            String Description = msg;
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel mChannel = new NotificationChannel(CHANNEL_ID, name, importance);
            mChannel.setDescription(Description);
            mChannel.enableLights(true);
            mChannel.setLightColor(Color.RED);
            mChannel.enableVibration(true);
            mChannel.setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION), audioAttributes);
            mChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
            mChannel.setShowBadge(false);
            notificationManager.createNotificationChannel(mChannel);
        }

        notificationManager.notify((int) System.currentTimeMillis() / 3600, builder.build());
    }

    private void showBlockedSmsNotification(Context context, String msg, String address) {
        Intent notificationIntent;
        String name = getContactName(context, address);
        notificationIntent = new Intent(context, MessageDetailsActivity.class);

        notificationIntent.putExtra("header", address);
        notificationIntent.putExtra("body", msg);
        notificationIntent.putExtra("reply", true);
        notificationIntent.putExtra("name", name);
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

        PendingIntent contentIntent = PendingIntent.getActivity(context, 0, notificationIntent,
                PendingIntent.FLAG_UPDATE_CURRENT);


        NotificationCompat.Builder builder =
                new NotificationCompat.Builder(context)
                        .setContentTitle("SMS blocked: " + name)
                        .setContentText(msg)
                        .setSmallIcon(R.drawable.ic_stat_name)
                        .setAutoCancel(true)
                        .setStyle(new NotificationCompat.BigTextStyle())
                        .setColor(ContextCompat.getColor(context, R.color.colorPrimaryDark))
                        .setChannelId(CHANNEL_ID)
                        .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                        .setContentIntent(contentIntent)
                        //.setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                        .setDefaults(Notification.DEFAULT_LIGHTS | Notification.DEFAULT_VIBRATE)
                        .setPriority(NotificationManager.IMPORTANCE_HIGH);
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            AudioAttributes audioAttributes = new AudioAttributes.Builder()
                    .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
                    .setUsage(AudioAttributes.USAGE_ALARM)
                    .build();
            String Description = msg;
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel mChannel = new NotificationChannel(CHANNEL_ID, name, importance);
            mChannel.setDescription(Description);
            mChannel.enableLights(true);
            mChannel.setLightColor(Color.RED);
            mChannel.enableVibration(true);
            mChannel.setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION), audioAttributes);
            mChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
            mChannel.setShowBadge(false);
            notificationManager.createNotificationChannel(mChannel);
        }

        notificationManager.notify(0, builder.build());
    }
}
