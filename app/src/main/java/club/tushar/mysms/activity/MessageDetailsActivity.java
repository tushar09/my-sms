package club.tushar.mysms.activity;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;

import androidx.appcompat.app.AlertDialog;
import androidx.databinding.DataBindingUtil;
import android.net.Uri;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.provider.Telephony;
import android.telephony.PhoneNumberUtils;
import android.telephony.SmsManager;
import android.telephony.SubscriptionInfo;
import android.telephony.SubscriptionManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.konifar.fab_transformation.FabTransformation;

import org.greenrobot.eventbus.EventBus;

import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.List;

import club.tushar.mysms.R;
import club.tushar.mysms.adapters.SMSDetailsAdapter;
import club.tushar.mysms.databinding.ActivityMessageDetailsBinding;
import club.tushar.mysms.eventBus.OnMessage;
import club.tushar.mysms.models.SMSDetailsModel;
import club.tushar.mysms.utils.Constants;
import club.tushar.mysms.utils.PermissionCheck;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class MessageDetailsActivity extends AppCompatActivity{

    private static String SENT = "SMS_SENT", DELIVERED = "SMS_DELIVERED";

    private ActivityMessageDetailsBinding binding;

    private String header;
    private String body;
    private String name;
    private boolean supportReply = false;

    private List<SMSDetailsModel> smsDetailsModels;

    private SMSDetailsAdapter adapter;

    private int totalSim = 0;

    private SubscriptionInfo simInfo1;
    private SubscriptionInfo simInfo2;

    private PendingIntent sentPI;
    private PendingIntent deliveredPI;
    private ArrayList<PendingIntent> sentPIList;
    private ArrayList<PendingIntent> deliveredPIList;
    private BroadcastReceiver sendSMS;
    private BroadcastReceiver deliverSMS;

    private String threadId;
    private String sentText;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_message_details);

        header = getIntent().getStringExtra("header");
        body = getIntent().getStringExtra("body");
        name = getIntent().getStringExtra("name");
        //Toast.makeText(this, getIntent().getStringExtra("sms_body") + " fsg", Toast.LENGTH_SHORT).show();
        supportReply = getIntent().getBooleanExtra("reply", true);


        sentPIList = new ArrayList<>();
        deliveredPIList = new ArrayList<>();

        sentPI = PendingIntent.getBroadcast(this, 0, new Intent(
                SENT), 0);

        deliveredPI = PendingIntent.getBroadcast(this, 0,
                new Intent(DELIVERED), 0);

        sentPIList.add(sentPI);
        deliveredPIList.add(deliveredPI);


        if(supportReply){
            binding.llNoReplyHolder.setVisibility(View.GONE);
            binding.llSendSmsHolder.setVisibility(View.VISIBLE);
        }else{
            binding.llNoReplyHolder.setVisibility(View.VISIBLE);
            binding.llSendSmsHolder.setVisibility(View.GONE);
        }

        getSupportActionBar().setTitle(((name == null) || (name.equals("")) ? header:name));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        if(body != null){
            markSmsAsRead(header, body);
        }



        smsDetailsModels = new ArrayList<>();

        adapter = new SMSDetailsAdapter(this, smsDetailsModels);
        binding.lvSms.setAdapter(adapter);

        if(header != null){
            if(PermissionCheck.readSms(this)){
                getSms(header);
            }
        }else{
            binding.etText.setText(getIntent().getStringExtra("sms_body"));
            processIntentData(getIntent());
        }
        if(header == null){
            startActivityForResult(new Intent(this, ContactsChooserActivity.class), 10);
        }

        sendSMS = new BroadcastReceiver() {
            @Override
            public void onReceive(Context arg0, Intent arg1) {
                switch (getResultCode())
                {
                    case Activity.RESULT_OK:
                        Toast.makeText(getBaseContext(), "SMS sent",
                                Toast.LENGTH_SHORT).show();
                        break;
                    case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
                        Toast.makeText(getBaseContext(), "Generic failure",
                                Toast.LENGTH_SHORT).show();
                        break;
                    case SmsManager.RESULT_ERROR_NO_SERVICE:
                        Toast.makeText(getBaseContext(), "No service",
                                Toast.LENGTH_SHORT).show();
                        break;
                    case SmsManager.RESULT_ERROR_NULL_PDU:
                        Toast.makeText(getBaseContext(), "Null PDU",
                                Toast.LENGTH_SHORT).show();
                        break;
                    case SmsManager.RESULT_ERROR_RADIO_OFF:
                        Toast.makeText(getBaseContext(), "Radio off",
                                Toast.LENGTH_SHORT).show();
                        break;
                }
            }
        };

        // DELIVERY BroadcastReceiver
        deliverSMS = new BroadcastReceiver() {
            @Override
            public void onReceive(Context arg0, Intent arg1) {
                switch (getResultCode()) {
                    case Activity.RESULT_OK:
                        saveSms(MessageDetailsActivity.this, header, sentText, 0, System.currentTimeMillis(), "sent");
                        Toast.makeText(getBaseContext(), R.string.sms_delivered,
                                Toast.LENGTH_SHORT).show();
                        break;
                    case Activity.RESULT_CANCELED:
                        Toast.makeText(getBaseContext(), R.string.sms_not_delivered,
                                Toast.LENGTH_SHORT).show();
                        break;
                }
            }
        };

        registerReceiver(sendSMS, new IntentFilter(SENT));
        registerReceiver(deliverSMS, new IntentFilter(DELIVERED));

        if(PermissionCheck.readPhoneState(MessageDetailsActivity.this)){
            SubscriptionManager localSubscriptionManager = SubscriptionManager.from(MessageDetailsActivity.this);
            if(localSubscriptionManager.getActiveSubscriptionInfoCount() > 1){
                List localList = localSubscriptionManager.getActiveSubscriptionInfoList();

                simInfo1 = (SubscriptionInfo) localList.get(0);
                simInfo2 = (SubscriptionInfo) localList.get(1);

//                Log.e("info", simInfo1.getNumber() + "number" + "");
//                Log.e("info", simInfo1.getCarrierName() + "");
//                Log.e("info", simInfo1.getDisplayName() + "");
//                Log.e("info", simInfo1.getSimSlotIndex() + "");
//                Log.e("info", simInfo1.getSubscriptionId() + "" + "");
//
//                Log.e("info 2", simInfo2.getNumber() + " number" + "");
//                Log.e("info 2", simInfo2.getCarrierName() + "");
//                Log.e("info 2", simInfo2.getDisplayName() + "");
//                Log.e("info 2", simInfo2.getSimSlotIndex() + "");
//                Log.e("info 2", simInfo2.getSubscriptionId() + "" + "");

                binding.btSimOne.setText(simInfo1.getDisplayName());
                binding.btSimTwo.setText(simInfo2.getDisplayName());

                //SendSMS From SIM One
                //SmsManager.getSmsManagerForSubscriptionId(simInfo1.getSubscriptionId()).sendTextMessage(customer.getMobile(), null, smsText, sentPI, deliveredPI);

                //SendSMS From SIM Two
                //SmsManager.getSmsManagerForSubscriptionId(simInfo2.getSubscriptionId()).sendTextMessage(customer.getMobile(), null, smsText, sentPI, deliveredPI);
            }
        }

        binding.fab.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                FabTransformation.with(binding.rlSimButtonHolder).transformTo(binding.llSimHolder);
            }
        });

        //binding.fab

        binding.ibSend.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                if(isDefaultApp()){
                    sentText = binding.etText.getText().toString();
                    if(sentText.length() >= 160){
                        sendLongSMS();
                    }else {
                        sendShortSMS();
                    }
                    SMSDetailsModel smsDetailsModel = new SMSDetailsModel();
                    smsDetailsModel.setBody(binding.etText.getText().toString());
                    smsDetailsModel.setDate(System.currentTimeMillis() + "");
                    smsDetailsModel.setType((byte) 0);

                    smsDetailsModels.add(smsDetailsModel);

                    adapter.notifyDataSetChanged();

                    //saveSms(MessageDetailsActivity.this, header, binding.etText.getText().toString(), 0, System.currentTimeMillis(), "sent");
                }else {
                    new AlertDialog.Builder(MessageDetailsActivity.this)
                            .setTitle("Default SMS App")
                            .setMessage("My SMS is not default SMS app. Please select it as default SMS app in order to send SMS.")

                            // Specifying a listener allows you to take an action before dismissing the dialog.
                            // The dialog is automatically dismissed when a dialog button is clicked.
                            .setPositiveButton("Set it", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    Intent intent = new Intent(Telephony.Sms.Intents.ACTION_CHANGE_DEFAULT);
                                    intent.putExtra(Telephony.Sms.Intents.EXTRA_PACKAGE_NAME, getPackageName());
                                    startActivity(intent);
                                }
                            })

                            // A null listener allows the button to dismiss the dialog and take no further action.
                            .setNegativeButton(android.R.string.no, null)
                            .setIcon(R.mipmap.ic_launcher)
                            .show();
                }

            }
        });

        binding.btSimOne.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                binding.tvSimNumber.setText("1");
                if (binding.rlSimButtonHolder.getVisibility() != View.VISIBLE) {
                    FabTransformation.with(binding.rlSimButtonHolder).transformFrom(binding.llSimHolder);
                    return;
                }
            }
        });

        binding.btSimTwo.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                binding.tvSimNumber.setText("2");
                if (binding.rlSimButtonHolder.getVisibility() != View.VISIBLE) {
                    FabTransformation.with(binding.rlSimButtonHolder).transformFrom(binding.llSimHolder);
                    return;
                }
            }
        });

    }

    private boolean isDefaultApp() {
        final String myPackageName = getPackageName();
        if (Telephony.Sms.getDefaultSmsPackage(getApplicationContext()).equals(myPackageName)) {
            return true;
        }
        return false;
    }

    private void loadSimInfo(){
        try {
            SubscriptionManager localSubscriptionManager = SubscriptionManager.from(MessageDetailsActivity.this);
            List localList = localSubscriptionManager.getActiveSubscriptionInfoList();

            simInfo1 = (SubscriptionInfo) localList.get(0);
            simInfo2 = (SubscriptionInfo) localList.get(1);

            binding.btSimOne.setText(simInfo1.getDisplayName());
            binding.btSimTwo.setText(simInfo2.getDisplayName());
        }catch (Exception e){
            Toast.makeText(this, getString(R.string.something_wrong), Toast.LENGTH_SHORT).show();
        }

    }

    public void sendShortSMS() {
        try {
            if(binding.tvSimNumber.getText().toString().equals("1")){
                if(simInfo1 != null){
                    SmsManager.getSmsManagerForSubscriptionId(simInfo1.getSubscriptionId()).sendTextMessage(header, null, binding.etText.getText().toString(), sentPI, deliveredPI);;
                }else {
                    Toast.makeText(this, getString(R.string.sim_one_error), Toast.LENGTH_SHORT).show();
                }
                Log.e("sent sort 1", "short");
            }else if(binding.tvSimNumber.getText().toString().equals("2")){
                if(simInfo2 != null){
                    SmsManager.getSmsManagerForSubscriptionId(simInfo2.getSubscriptionId()).sendTextMessage(header, null, binding.etText.getText().toString(), sentPI, deliveredPI);;
                }else {
                    Toast.makeText(this, getString(R.string.sim_two_error), Toast.LENGTH_SHORT).show();
                }
                Log.e("sent sort 2", "short");
            }
        } catch (Exception ex) {
            Toast.makeText(getApplicationContext(),ex.getMessage().toString(),
                    Toast.LENGTH_LONG).show();
            ex.printStackTrace();
        }
    }

    public void sendLongSMS(){
        String phoneNumber = header;
        String message = binding.etText.getText().toString();
        SmsManager smsManager = SmsManager.getDefault();
        ArrayList<String> parts = smsManager.divideMessage(message);
            smsManager.sendMultipartTextMessage(phoneNumber, null, parts, null, null);

        if(binding.tvSimNumber.getText().toString().equals("1")){
            if(simInfo1 != null){
                SmsManager.getSmsManagerForSubscriptionId(simInfo1.getSubscriptionId()).sendMultipartTextMessage(header, null, parts, sentPIList, deliveredPIList);
            }else {
                Toast.makeText(this, getString(R.string.sim_one_error), Toast.LENGTH_SHORT).show();
            }
            Log.e("sent long 1", "long");
        }else if(binding.tvSimNumber.getText().toString().equals("2")){
            if(simInfo2 != null){
                SmsManager.getSmsManagerForSubscriptionId(simInfo2.getSubscriptionId()).sendMultipartTextMessage(header, null, parts, sentPIList, deliveredPIList);
            }else {
                Toast.makeText(this, getString(R.string.sim_two_error), Toast.LENGTH_SHORT).show();
            }
            Log.e("sent long 2", "long");
        }
    }

    public boolean saveSms(Context context, String phoneNumber, String message, int readState, long time, String folderName) {
        boolean ret = false;
        try {
            //Telephony.Sms.MESSAGE_TYPE_DRAFT
            ContentValues values = new ContentValues();
            values.put(Telephony.Sms.ADDRESS, phoneNumber);
            values.put(Telephony.Sms.BODY, message);
            values.put(Telephony.Sms.READ, readState); //"0" for have not read sms and "1" for have read sms
            values.put(Telephony.Sms.DATE, time);
            values.put(Telephony.Sms.TYPE, 0);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                Uri uri = Telephony.Sms.Sent.CONTENT_URI;
                context.getContentResolver().insert(uri, values);
            }
            else {
                context.getContentResolver().insert(Uri.parse("content://sms/" + folderName), values);
            }

            ret = true;
        } catch (Exception ex) {
            ex.printStackTrace();
            Log.e("ex", ex.toString());
            ret = false;
        }
        return ret;
    }

    private void getSms(String header){
        Uri uriSMSURI = Uri.parse("content://sms/");
        ContentResolver cr = getContentResolver();

        Cursor c = cr.query(uriSMSURI, null, null, null, null);
        startManagingCursor(c);
        if(c.moveToFirst()){
            c.moveToLast();
            do{
                String address = c.getString(c.getColumnIndexOrThrow("address"));
                if(address.equals(header)){
                    if(threadId == null){
                        threadId = c.getString(1);
                    }
                    SMSDetailsModel smsDetailsModel = new SMSDetailsModel();
                    smsDetailsModel.setBody(c.getString(c.getColumnIndexOrThrow("body")));
                    smsDetailsModel.setDate(c.getString(c.getColumnIndexOrThrow("date")));
                    smsDetailsModel.setId(c.getString(c.getColumnIndexOrThrow("_id")));
                    smsDetailsModel.setThread_id(threadId);
                    if(c.getString(c.getColumnIndexOrThrow("type")).contains("1")){
                        smsDetailsModel.setType((byte) 1);
                    }else{
                        smsDetailsModel.setType((byte) 0);
                    }
                    //Log.e("threadID", "s " + c.getString(c.getColumnIndexOrThrow("thread_id")));
                    //Log.e("threadID", "s " + c.getColumnName(1));
                    //Log.e("threadID", "s " + c.getString(0) + " " + getString(0));
                    smsDetailsModels.add(smsDetailsModel);

                }
                adapter.notifyDataSetChanged();
            }
            while(c.moveToPrevious());


        }

    }

    public void markSmsAsRead(final String from, final String body){

        Uri uri = Uri.parse("content://sms/inbox");
        String selection = "address = ? AND body = ? AND read = ?";
        String[] selectionArgs = {from, body, "0"};

        ContentValues values = new ContentValues();
        values.put("read", true);

        getContentResolver().update(uri, values, selection, selectionArgs);
    }

    private void processIntentData(Intent intent) {
        if (null == intent){
            //Toast.makeText(this, "dsfg", Toast.LENGTH_SHORT).show();
            return;
        }

        if (Intent.ACTION_SENDTO.equals(intent.getAction())) {
            //in the data i'll find the number of the destination
            String destionationNumber = intent.getDataString();
            destionationNumber = URLDecoder.decode(destionationNumber);
            //clear the string
            destionationNumber = destionationNumber.replace("-", "")
                    .replace("smsto:", "")
                    .replace("sms:", "");
            //and set fields
            header = destionationNumber;
            name = getContactName(destionationNumber)[0];
            getSupportActionBar().setTitle(name);
            if(PermissionCheck.readSms(this)){
                getSms(header);
            }
            supportReply = true;
            body = null;
            //Toast.makeText(this, destionationNumber + "dsfg", Toast.LENGTH_SHORT).show();
            binding.etText.setText(intent.getStringExtra("sms_body"));

        } else if (Intent.ACTION_SEND.equals(intent.getAction()) && "text/plain".equals(intent.getType())) {
            //in the data i'll find the content of the message
            //Toast.makeText(this,  "send", Toast.LENGTH_SHORT).show();
            String message = intent.getStringExtra(Intent.EXTRA_TEXT);
            binding.etText.setText(message);
            //startActivityForResult(new Intent(this, ContactsChooserActivity.class), 10);
            //clear the string
            Log.e("dest", message);
        }
    }

    public String []  getContactName(final String phoneNumber) {
        Uri uri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(phoneNumber));

        String[] projection = new String[]{ContactsContract.PhoneLookup.DISPLAY_NAME, ContactsContract.PhoneLookup.PHOTO_URI};

        String contactName = phoneNumber;
        String photo = "s";
        Cursor cursor = getContentResolver().query(uri, projection, null, null, null);

        if (cursor != null) {
            if (cursor.moveToFirst()) {
                contactName = cursor.getString(0);
                photo = cursor.getString(1);
            }
            if(photo != null){
                Log.e("photo", photo);
            }
            cursor.close();
        }
        return new String[]{contactName, photo};
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        switch(item.getItemId()){
            case android.R.id.home:
                // app icon in action bar clicked; go home
                finish();
                return true;
            case R.id.action_call:
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:" + Uri.encode(header)));
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.actionbar_menu, menu);
        if (!PhoneNumberUtils.isGlobalPhoneNumber(header)) {
            menu.findItem(R.id.action_call).setVisible(false);
        }

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public void onBackPressed() {
        if (binding.rlSimButtonHolder.getVisibility() != View.VISIBLE) {
            FabTransformation.with(binding.rlSimButtonHolder).transformFrom(binding.llSimHolder);
            return;
        }
        super.onBackPressed();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults){
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(Constants.READ_PHONE_STATE == requestCode){
           loadSimInfo();
        }
        if (Constants.READ_SMS_REQUEST == requestCode) {
            getSms(header);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 10) {
            if (resultCode == RESULT_OK) {
                name = data.getStringExtra("name");
                header = data.getStringExtra("number");
                binding.llNoReplyHolder.setVisibility(View.GONE);
                binding.llSendSmsHolder.setVisibility(View.VISIBLE);
                getSupportActionBar().setTitle(((name == null) || (name.equals("")) ? header:name));
                if(PermissionCheck.readSms(this)){
                    getSms(header);
                }
            }
            if (resultCode == RESULT_CANCELED) {
                //Write your code if there's no result
            }
        }
    }//onActivityResult

    @Override
    protected void onDestroy(){
        super.onDestroy();
        unregisterReceiver(deliverSMS);
        unregisterReceiver(sendSMS);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
