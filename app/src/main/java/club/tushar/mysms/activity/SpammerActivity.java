package club.tushar.mysms.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.os.Bundle;
import android.util.Log;

import club.tushar.mysms.R;
import club.tushar.mysms.adapters.SpammerAdapter;
import club.tushar.mysms.databinding.ActivityHomeBinding;
import club.tushar.mysms.databinding.ActivitySpammerBinding;
import club.tushar.mysms.utils.Constants;

public class SpammerActivity extends AppCompatActivity {

    private ActivitySpammerBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_spammer);

        getSupportActionBar().setTitle(R.string.spammer);

        Log.e("list", Constants.getDbHelper(this).getSpammers().size() + "");
        binding.rvList.setLayoutManager(new LinearLayoutManager(this));
        binding.rvList.setAdapter(new SpammerAdapter(this, Constants.getDbHelper(this).getSpammers()));
    }
}
