package club.tushar.mysms.activity;

import android.content.Intent;
import android.provider.Telephony;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import club.tushar.mysms.R;

public class StarterActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_starter);
        //Telephony.Sms.getDefaultSmsPackage(this);
    }

    @Override
    protected void onResume() {
        super.onResume();

        final String myPackageName = getPackageName();
        if (Telephony.Sms.getDefaultSmsPackage(getApplicationContext()) == null){
            startActivity(new Intent(this, HomeActivity.class));
            finish();
        }else {
            if (!Telephony.Sms.getDefaultSmsPackage(getApplicationContext()).equals(myPackageName)) {
                // App is not default.
                // Show the "not currently set as the default SMS app" interface

                // Set up a button that allows the user to change the default SMS app
                Button button = findViewById(R.id.bt_set_default);
                button.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        Intent intent = new Intent(Telephony.Sms.Intents.ACTION_CHANGE_DEFAULT);
                        intent.putExtra(Telephony.Sms.Intents.EXTRA_PACKAGE_NAME, myPackageName);
                        startActivity(intent);
                    }
                });
            } else {
                // App is the default.
                // Hide the "not currently set as the default SMS app" interface
                startActivity(new Intent(this, HomeActivity.class));
                finish();
            }
        }

    }
}
