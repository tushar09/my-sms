package club.tushar.mysms.activity;

import androidx.databinding.DataBindingUtil;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;


import java.net.URLDecoder;

import club.tushar.mysms.R;
import club.tushar.mysms.databinding.ActivitySendSmsBinding;

public class SendSMSActivity extends AppCompatActivity {

    private ActivitySendSmsBinding binding;

    private String TAG = "test";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_send_sms);
        processIntentData(getIntent());

    }

    private void processIntentData(Intent intent) {
        if (null == intent) return;

        if (Intent.ACTION_SENDTO.equals(intent.getAction())) {
            //in the data i'll find the number of the destination
            String destionationNumber = intent.getDataString();
            destionationNumber = URLDecoder.decode(destionationNumber);
            //clear the string
            destionationNumber = destionationNumber.replace("-", "")
                    .replace("smsto:", "")
                    .replace("sms:", "");
            //and set fields
            Log.e("dest", destionationNumber);

        } else if (Intent.ACTION_SEND.equals(intent.getAction()) && "text/plain".equals(intent.getType())) {
            //in the data i'll find the content of the message
            String message = intent.getStringExtra(Intent.EXTRA_TEXT);
            //clear the string
            Log.e("dest", message);
        }
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        //getContacts();

    }

    private void getContacts() {
//        Cursor phones = this.getContentResolver().query(ContactsContract.Contacts.CONTENT_URI, null,null,null, null);
//
//        // loop over all contacts
//        if(phones != null) {
//            while (phones.moveToNext()) {
//                // get contact info
//                String phoneNumber = null;
//                String id = phones.getString(phones.getColumnIndex(ContactsContract.Contacts._ID));
//                String name = phones.getString(phones.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
//                String avatarUriString = phones.getString(phones.getColumnIndex(ContactsContract.Contacts.PHOTO_THUMBNAIL_URI));
//                Uri avatarUri = null;
//                if(avatarUriString != null)
//                    avatarUri = Uri.parse(avatarUriString);
//
//                // get phone number
//                if (Integer.parseInt(phones.getString(phones.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))) > 0) {
//                    Cursor pCur = this.getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
//                            null,
//                            ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?", new String[] { id }, null);
//
//                    while (pCur != null && pCur.moveToNext()) {
//                        phoneNumber = pCur.getString(pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
//                    }
//
//                    pCur.close();
//
//                }
//
//                ContactChip contactChip = new ContactChip(id, avatarUri, name, phoneNumber);
//                // add contact to the list
//                mContactList.add(contactChip);
//            }
//            phones.close();
//            Log.e(TAG, "Done");
//        }

        // pass contact list to chips input

    }
}
