package club.tushar.mysms.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.os.Bundle;

import club.tushar.mysms.R;
import club.tushar.mysms.adapters.SpamBoxAdapter;
import club.tushar.mysms.utils.Constants;

public class SpamBoxActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_spam_box);
        getSupportActionBar().setTitle(getIntent().getStringExtra("name"));

        RecyclerView recyclerView = findViewById(R.id.rv_list);
        recyclerView.setAdapter(new SpamBoxAdapter(this, Constants.getDbHelper(this).getSpamBoxBySpammerId(getIntent().getLongExtra("id", -1))));
        recyclerView.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, true));
    }
}
