package club.tushar.mysms.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.core.view.MenuItemCompat;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.animation.Animator;
import android.animation.LayoutTransition;
import android.animation.ObjectAnimator;
import android.app.SearchManager;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.CustomEvent;

import org.greenrobot.eventbus.EventBus;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import club.tushar.mysms.R;
import club.tushar.mysms.adapters.ChooseContactsAdapter;
import club.tushar.mysms.databinding.ActivityContactsChooserBinding;
import club.tushar.mysms.models.Contacts;
import club.tushar.mysms.utils.Constants;
import club.tushar.mysms.utils.PermissionCheck;
import io.fabric.sdk.android.Fabric;

public class ContactsChooserActivity extends AppCompatActivity {

    private ActivityContactsChooserBinding binding;

    private SearchManager searchManager;
    private Menu menu;
    private SearchView searchView;
    private MenuItem actionbarMenu;

    private List<Contacts> contacts;
    private ChooseContactsAdapter mainAdapterRv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_contacts_chooser);

        if(PermissionCheck.readAndWriteContacts(this)){
            contacts = readContactsAndStore();
            mainAdapterRv = new ChooseContactsAdapter(this, contacts);

            binding.rvContacts.setLayoutManager(new LinearLayoutManager(this));
            binding.rvContacts.setAdapter(mainAdapterRv);

            getSupportActionBar().setTitle("Select Person");
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (Constants.READ_WRITE_CONTACTS_REQUEST == requestCode) {
            contacts = readContactsAndStore();
            mainAdapterRv = new ChooseContactsAdapter(this, contacts);

            binding.rvContacts.setLayoutManager(new LinearLayoutManager(this));
            binding.rvContacts.setAdapter(mainAdapterRv);

            getSupportActionBar().setTitle("Select Person");
        }
    }

    private List<Contacts> readContactsAndStore() {

        ArrayList<String> duplicates = new ArrayList<>();

        List<Contacts> list = new ArrayList<>();
        Cursor phones = getContentResolver().query(
                ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, null,
                null, null);
        try {
            if (phones != null) {
                while (phones.moveToNext()) {
                    String name = phones
                            .getString(phones
                                    .getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
                    String number = phones
                            .getString(phones
                                    .getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                    String id = phones
                            .getString(phones
                                    .getColumnIndex(ContactsContract.CommonDataKinds.Phone.RAW_CONTACT_ID));
                    String contact_id = phones
                            .getString(phones
                                    .getColumnIndex(ContactsContract.CommonDataKinds.Phone.CONTACT_ID));
                    String image_uri = phones
                            .getString(phones
                                    .getColumnIndex(ContactsContract.CommonDataKinds.Phone.PHOTO_URI));
                    //number = NumberActions.format(number);
                    //init contacts
                    String s = number.replaceAll("-", "").replaceAll(" ", "") + name;
                    if (duplicates.contains(s)) {

                    } else {
                        duplicates.add(s);
                        Contacts c = new Contacts();
                        c.set_id(id);
                        c.setContact_id(contact_id);
                        c.setSynced(0);
                        c.setName(name);
                        c.setNumber(number);
                        c.setImage_uri(image_uri);

                        list.add(c);

                    }

                }
            } else {
                Toast.makeText(this, "Could Not Read Contacts", Toast.LENGTH_SHORT).show();
            }

        } catch (Exception e) {

        }

        //new MainActivity.StoreContactsToServer().execute(list);

        Collections.sort(list, new Comparator<Contacts>() {
            @Override
            public int compare(Contacts contacts, Contacts t1) {
                try {
                    return contacts.getName().compareToIgnoreCase(t1.getName());
                } catch (Exception e) {
                    return 0;
                }

            }
        });

        return list;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        getMenuInflater().inflate(R.menu.search, menu);
        activateSearchMenu(menu, searchManager);
        return super.onCreateOptionsMenu(menu);
    }

    private void activateSearchMenu(Menu menu, SearchManager searchManager) {
        this.menu = menu;
        actionbarMenu = menu.findItem(R.id.action_search);
        actionbarMenu.setVisible(true);
        searchView = (SearchView) menu.findItem(R.id.action_search).getActionView();
        EditText searchTextView = (EditText) searchView.findViewById(androidx.appcompat.R.id.search_src_text);
        searchTextView.setTextColor(Color.WHITE);

        try {
            Field mCursorDrawableRes = TextView.class.getDeclaredField("mCursorDrawableRes");
            mCursorDrawableRes.setAccessible(true);
            mCursorDrawableRes.set(searchTextView, R.drawable.custom_edit_text_cursor); //This sets the cursor resource ID to 0 or @null which will make it visible on white background
        } catch (Exception e) {
        }

        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        LinearLayout searchBar = (LinearLayout) searchView.findViewById(R.id.search_bar);
        LayoutTransition transitioner = new LayoutTransition();
        Animator appearingAnimation = ObjectAnimator.ofFloat(null, "translationX", 600, 0);
        transitioner.setAnimator(LayoutTransition.APPEARING, appearingAnimation);
        transitioner.setDuration(LayoutTransition.APPEARING, 700);
        transitioner.setStartDelay(LayoutTransition.APPEARING, 0);
        searchBar.setLayoutTransition(transitioner);
        MenuItem mi = menu.findItem(R.id.action_search);
        MenuItemCompat.setOnActionExpandListener(mi,
                new MenuItemCompat.OnActionExpandListener() {
                    @Override
                    public boolean onMenuItemActionCollapse(MenuItem item) {
                        if (contacts != null) {
                            contacts.clear();
                            contacts.addAll(readContactsAndStore());
                        }

                        if (mainAdapterRv != null) {
                            mainAdapterRv.notifyDataSetChanged();
                        }
                        return true; // Return true to collapse action view
                    }

                    @Override
                    public boolean onMenuItemActionExpand(MenuItem item) {
                        return true; // Return true to expand action view
                    }
                });

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                search(newText.toLowerCase());
                //Fabric.with(ContactsChooserActivity.this).in
                //Answers.getInstance().logCustom(new CustomEvent("Search").putCustomAttribute("Query", newText));
                return true;
            }
        });
    }

    private void search(String terms) {
        if (contacts != null && contacts.size() != 0) {
            List<Contacts> list = new ArrayList<>();
            for (int t = 0; t < contacts.size(); t++) {
                if (contacts.get(t).getName() != null && contacts.get(t).getNumber() != null) {
                    if (contacts.get(t).getName().toLowerCase().contains(terms.toLowerCase()) || contacts.get(t).getNumber().replaceAll(" ", "").contains(terms.toLowerCase())) {
                        list.add(contacts.get(t));
                    }
                }
            }
            mainAdapterRv = new ChooseContactsAdapter(this, list);
            binding.rvContacts.setAdapter(mainAdapterRv);
        }
    }
}
