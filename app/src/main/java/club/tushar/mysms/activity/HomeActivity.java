package club.tushar.mysms.activity;

import android.animation.Animator;
import android.animation.LayoutTransition;
import android.animation.ObjectAnimator;
import android.app.SearchManager;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;

import androidx.appcompat.widget.SearchView;
import androidx.core.view.MenuItemCompat;
import androidx.databinding.DataBindingUtil;

import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Handler;
import android.provider.ContactsContract;
import android.provider.Telephony;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.os.Bundle;
import android.telephony.PhoneNumberUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.CustomEvent;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.webrtc.Camera1Enumerator;
import org.webrtc.CameraEnumerator;
import org.webrtc.DefaultVideoDecoderFactory;
import org.webrtc.DefaultVideoEncoderFactory;
import org.webrtc.EglBase;
import org.webrtc.MediaConstraints;
import org.webrtc.PeerConnectionFactory;
import org.webrtc.SurfaceTextureHelper;
import org.webrtc.VideoCapturer;
import org.webrtc.VideoSource;
import org.webrtc.VideoTrack;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.List;

import club.tushar.mysms.R;
import club.tushar.mysms.adapters.ChooseContactsAdapter;
import club.tushar.mysms.adapters.MainAdapter;
import club.tushar.mysms.adapters.MainAdapterRv;
import club.tushar.mysms.databinding.ActivityHomeBinding;
import club.tushar.mysms.eventBus.OnMessage;
import club.tushar.mysms.models.Contacts;
import club.tushar.mysms.models.SMSModelsDto;
import club.tushar.mysms.utils.Constants;
import club.tushar.mysms.utils.PermissionCheck;
import io.fabric.sdk.android.Fabric;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class HomeActivity extends AppCompatActivity {

    private ActivityHomeBinding binding;

    private boolean readWrite = false;

    private List<SMSModelsDto> sms;
    private LinkedHashMap<String, SMSModelsDto> stringSMSModelsDtoHashMap;
    private SMSModelsDto smsModelsDto;

    private MainAdapterRv adapter;
    private Cursor cursor;
    private Cursor c;

    private SearchManager searchManager;
    private Menu menu;
    private SearchView searchView;
    private MenuItem actionbarMenu;
    private ArrayList smsSearch;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        binding = DataBindingUtil.setContentView(this, R.layout.activity_home);

        sms = new ArrayList<>();
        stringSMSModelsDtoHashMap = new LinkedHashMap<String, SMSModelsDto>();
        adapter = new MainAdapterRv(this, sms);
        adapter.setHasStableIds(true);
        binding.lvSms.setLayoutManager(new Linear(this));
        binding.lvSms.setAdapter(adapter);

        binding.fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(new Intent(HomeActivity.this, ContactsChooserActivity.class), 10);
            }
        });

        PeerConnectionFactory.InitializationOptions.Builder optionBuilder = PeerConnectionFactory.InitializationOptions.builder(this);
        optionBuilder.setEnableInternalTracer(true);
        optionBuilder.setFieldTrials("WebRTC-FlexFEC-03/Enabled/");
        PeerConnectionFactory.initialize(optionBuilder.createInitializationOptions());

        EglBase rootEglBase = EglBase.create();
        DefaultVideoEncoderFactory defaultVideoEncoderFactory = new DefaultVideoEncoderFactory(
                rootEglBase.getEglBaseContext(),  /* enableIntelVp8Encoder */true,  /* enableH264HighProfile */true);
        DefaultVideoDecoderFactory defaultVideoDecoderFactory = new DefaultVideoDecoderFactory(rootEglBase.getEglBaseContext());

        PeerConnectionFactory peerConnectionFactory = PeerConnectionFactory.builder()
                .setOptions(new PeerConnectionFactory.Options())
                .setVideoEncoderFactory(defaultVideoEncoderFactory)
                .setVideoDecoderFactory(defaultVideoDecoderFactory)
                .createPeerConnectionFactory();

        //Now create a VideoCapturer instance. Callback methods are there if you want to do something! Duh!
        VideoCapturer videoCapturerAndroid = createVideoCapturer();

        //Create MediaConstraints - Will be useful for specifying video and audio constraints. More on this later!
        MediaConstraints constraints = new MediaConstraints();


        //Create a VideoSource instance
        VideoSource videoSource;
        VideoTrack localVideoTrack;
        if(videoCapturerAndroid != null){
            SurfaceTextureHelper surfaceTextureHelper = SurfaceTextureHelper.create("CaptureThread", rootEglBase.getEglBaseContext());
            videoSource = peerConnectionFactory.createVideoSource(videoCapturerAndroid.isScreencast());
            localVideoTrack = peerConnectionFactory.createVideoTrack("100", videoSource);
        }


    }

    private VideoCapturer createVideoCapturer() {
        VideoCapturer videoCapturer;
        videoCapturer = createCameraCapturer(new Camera1Enumerator(false));
        return videoCapturer;
    }

    private VideoCapturer createCameraCapturer(CameraEnumerator enumerator) {
        final String[] deviceNames = enumerator.getDeviceNames();

        // Trying to find a front facing camera!
        for (String deviceName : deviceNames) {
            if (enumerator.isFrontFacing(deviceName)) {
                VideoCapturer videoCapturer = enumerator.createCapturer(deviceName, null);

                if (videoCapturer != null) {
                    return videoCapturer;
                }
            }
        }

        // We were not able to find a front cam. Look for other cameras
        for (String deviceName : deviceNames) {
            if (!enumerator.isFrontFacing(deviceName)) {
                VideoCapturer videoCapturer = enumerator.createCapturer(deviceName, null);
                if (videoCapturer != null) {
                    return videoCapturer;
                }
            }
        }

        return null;
    }

    private class Thred extends AsyncTask<String, String, String>{

        @Override
        protected void onProgressUpdate(String... values) {
            super.onProgressUpdate(values);
            Log.e("progre", values + "");
        }

        @Override
        protected String doInBackground(String... strings) {
            sms.clear();
            stringSMSModelsDtoHashMap.clear();
            if (PermissionCheck.readSms(HomeActivity.this)) {
                getSMS();
            }
            return null;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        getMenuInflater().inflate(R.menu.home, menu);
        activateSearchMenu(menu, searchManager);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == R.id.spam_box){
            startActivity(new Intent(this, SpammerActivity.class));
        }
        return super.onOptionsItemSelected(item);
    }

    public List<SMSModelsDto> getSMS() {
        Uri uriSMSURI = Uri.parse("content://sms/");
        ContentResolver cr = getContentResolver();

        c = cr.query(uriSMSURI, null, null, null, null);
        startManagingCursor(c);

        if (c.moveToFirst()) {
            for (int i = 0; i < c.getCount(); i++) {

                String header = c.getString(c.getColumnIndexOrThrow("address"));

                if (stringSMSModelsDtoHashMap.containsKey(header)) {
                    smsModelsDto = stringSMSModelsDtoHashMap.get(header);
                    smsModelsDto.setRepeatCount(smsModelsDto.getRepeatCount() + 1);
                    stringSMSModelsDtoHashMap.put(header, smsModelsDto);
                } else {
                    smsModelsDto = new SMSModelsDto();
                    smsModelsDto.setId(c.getString(c.getColumnIndexOrThrow("_id")));
                    String threadId = null;
                    threadId = c.getString(c.getColumnIndexOrThrow("thread_id"));;
                    if(threadId != null){
                        smsModelsDto.setThreadId(threadId);
                    }
                    smsModelsDto.setHeader(header);
                    smsModelsDto.setName(header);
                    smsModelsDto.setBody(c.getString(c.getColumnIndexOrThrow("body")));
                    smsModelsDto.setRead(Byte.parseByte(c.getString(c.getColumnIndex("read"))));
                    smsModelsDto.setDate(Long.parseLong(c.getString(c.getColumnIndexOrThrow("date"))));
                    if (c.getString(c.getColumnIndexOrThrow("type")).contains("1")) {
                        smsModelsDto.setType((byte) 1);
                    } else {
                        smsModelsDto.setType((byte) 0);
                    }

                    stringSMSModelsDtoHashMap.put(header, smsModelsDto);
                }


                if (sms.contains(smsModelsDto)) {
                    sms.set(sms.indexOf(smsModelsDto), smsModelsDto);
                } else {
                    sms.add(smsModelsDto);
                }
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        adapter.notifyDataSetChanged();
                    }
                });
                c.moveToNext();
            }

            if (PermissionCheck.readAndWriteContacts(HomeActivity.this)) {
                loadContactsData();
            }


        }
        return sms;

    }

    private void loadContactsData() {
        readWrite = true;
        for (int t = 0; t < sms.size(); t++) {
            if (PhoneNumberUtils.isGlobalPhoneNumber(sms.get(t).getHeader())) {
                SMSModelsDto smsModelsDto = sms.get(t);
                String[] nameAndPhoto = getContactName(smsModelsDto.getHeader());
                smsModelsDto.setName(nameAndPhoto[0]);
                smsModelsDto.setImage(nameAndPhoto[1]);
                smsModelsDto.setSupportReply(true);
                sms.set(t, smsModelsDto);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        adapter.notifyDataSetChanged();
                    }
                });

            }
        }

    }

    public String[] getContactName(final String phoneNumber) {
        Uri uri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(phoneNumber));

        String[] projection = new String[]{ContactsContract.PhoneLookup.DISPLAY_NAME, ContactsContract.PhoneLookup.PHOTO_URI};

        String contactName = phoneNumber;
        String photo = "s";
        cursor = getContentResolver().query(uri, projection, null, null, null);

        if (cursor != null) {
            if (cursor.moveToFirst()) {
                contactName = cursor.getString(0);
                photo = cursor.getString(1);
            }
            if (photo != null) {
                Log.e("photo", photo);
            }
            //cursor.close();
        }
        return new String[]{contactName, photo};
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 10) {
            if (resultCode == RESULT_OK) {
                String name = data.getStringExtra("name");
                String number = data.getStringExtra("number");

                startActivity(new Intent(this, MessageDetailsActivity.class)
                        .putExtra("header", number)
                        .putExtra("body", "")
                        .putExtra("reply", true)
                        .putExtra("name", name)
                );
            }
            if (resultCode == RESULT_CANCELED) {
                //Write your code if there's no result
            }
        }
    }//onActivityResult

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessage(OnMessage message) {
        SMSModelsDto smsModelsDto;
        if (stringSMSModelsDtoHashMap.containsKey(message.getSenderNumber())) {
            smsModelsDto = stringSMSModelsDtoHashMap.get(message.getSenderNumber());
            smsModelsDto.setRepeatCount(smsModelsDto.getRepeatCount() + 1);
            smsModelsDto.setBody(message.getMessageBody());
            smsModelsDto.setRead((byte) 0);
            smsModelsDto.setDate(message.getTime());
            stringSMSModelsDtoHashMap.put(message.getSenderNumber(), smsModelsDto);
            Log.e("he", "he");
        } else {
            smsModelsDto = new SMSModelsDto();
            smsModelsDto.setId(System.currentTimeMillis() + "");
            smsModelsDto.setHeader(message.getSenderNumber());
            String[] nameAndPhoto = getContactName(smsModelsDto.getHeader());
            smsModelsDto.setName(nameAndPhoto[0]);
            smsModelsDto.setImage(nameAndPhoto[1]);
            smsModelsDto.setBody(message.getMessageBody());
            smsModelsDto.setRead((byte) message.getReadStatus());
            smsModelsDto.setDate(message.getTime());
            smsModelsDto.setType((byte) 0);

            stringSMSModelsDtoHashMap.put(message.getSenderNumber(), smsModelsDto);

            Log.e("he2", "he2");
        }
        if (sms.contains(smsModelsDto)) {
            Log.e("he2 old", "he2");
            sms.set(sms.indexOf(smsModelsDto), smsModelsDto);

            Collections.sort(sms, new Comparator<SMSModelsDto>() {
                @Override
                public int compare(SMSModelsDto smsModelsDto, SMSModelsDto t1) {
                    //return Long.compare(smsModelsDto.getDate(), t1.getDate());
                    return (int) (t1.getDate() - smsModelsDto.getDate());
                }
            });
            adapter.notifyDataSetChanged();
        } else {
            sms.add(0, smsModelsDto);
            Collections.sort(sms, new Comparator<SMSModelsDto>() {
                @Override
                public int compare(SMSModelsDto smsModelsDto, SMSModelsDto t1) {
                    //return Long.compare(smsModelsDto.getDate(), t1.getDate());
                    return (int) (t1.getDate() - smsModelsDto.getDate());
                }
            });
            adapter.notifyDataSetChanged();
        }


    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        new Thred().execute();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        cursor.close();
        c.close();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (Constants.READ_SMS_REQUEST == requestCode) {
            getSMS();
        } else if (Constants.READ_WRITE_CONTACTS_REQUEST == requestCode) {
            loadContactsData();
        }
    }

    private void activateSearchMenu(Menu menu, SearchManager searchManager) {
        this.menu = menu;
        actionbarMenu = menu.findItem(R.id.action_search);
        actionbarMenu.setVisible(true);
        searchView = (SearchView) menu.findItem(R.id.action_search).getActionView();
        EditText searchTextView = (EditText) searchView.findViewById(androidx.appcompat.R.id.search_src_text);
        searchTextView.setTextColor(Color.WHITE);

        try {
            Field mCursorDrawableRes = TextView.class.getDeclaredField("mCursorDrawableRes");
            mCursorDrawableRes.setAccessible(true);
            mCursorDrawableRes.set(searchTextView, R.drawable.custom_edit_text_cursor); //This sets the cursor resource ID to 0 or @null which will make it visible on white background
        } catch (Exception e) {
        }

        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        LinearLayout searchBar = (LinearLayout) searchView.findViewById(R.id.search_bar);
        LayoutTransition transitioner = new LayoutTransition();
        Animator appearingAnimation = ObjectAnimator.ofFloat(null, "translationX", 600, 0);
        transitioner.setAnimator(LayoutTransition.APPEARING, appearingAnimation);
        transitioner.setDuration(LayoutTransition.APPEARING, 700);
        transitioner.setStartDelay(LayoutTransition.APPEARING, 0);
        searchBar.setLayoutTransition(transitioner);
        MenuItem mi = menu.findItem(R.id.action_search);
        MenuItemCompat.setOnActionExpandListener(mi,
                new MenuItemCompat.OnActionExpandListener() {
                    @Override
                    public boolean onMenuItemActionCollapse(MenuItem item) {

                        return true; // Return true to collapse action view
                    }

                    @Override
                    public boolean onMenuItemActionExpand(MenuItem item) {
                        smsSearch = new ArrayList();
                        adapter = new MainAdapterRv(HomeActivity.this, smsSearch);
                        adapter.setHasStableIds(true);
                        binding.lvSms.setLayoutManager(new Linear(HomeActivity.this));
                        binding.lvSms.setAdapter(adapter);
                        Log.e("espande", "Expanded");
                        return true; // Return true to expand action view
                    }
                });

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                search(newText.toLowerCase());
                Answers.getInstance().logCustom(new CustomEvent("Search").putCustomAttribute("Query", newText));
                return true;
            }
        });
    }

    private void search(String terms) {
        if (sms != null && sms.size() != 0) {
            smsSearch.clear();
            for (int t = 0; t < sms.size(); t++) {
                if (sms.get(t).getName().toLowerCase().contains(terms.toLowerCase()) || sms.get(t).getHeader().toLowerCase().contains(terms.toLowerCase())) {
                    smsSearch.add(sms.get(t));
                }
            }
            adapter.notifyDataSetChanged();
        }
    }

    private class Linear extends LinearLayoutManager {

        public Linear(Context context) {
            super(context);
        }

        @Override
        public boolean supportsPredictiveItemAnimations() {
            return true;
        }
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
