package club.tushar.mysms.utils;

import android.app.ProgressDialog;
import android.content.Context;
import android.util.Log;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import java.net.HttpURLConnection;
import java.util.concurrent.TimeUnit;

import club.tushar.mysms.db.DbHelper;

/**
 * Created by msi on 8/21/2017.
 */

public class Constants{
    public static final int CALL_REQUEST = 1;
    public static final int SEND_SMS_REQUEST = 2;
    public static final int READ_CONTACTS_REQUEST = 3;
    public static final int WRITE_CONTACTS_REQUEST = 4;
    public static final int ACTION_MANAGE_OVERLAY_PERMISSION = 5;
    public static final int READ_CALL_LOG = 6;
    public static final int READ_WRITE_EXTERNAL_STORAGE_REQUEST = 7;
    public static final int RECORD_AUDIO_REQUEST = 8;
    public static final int GET_ACCOUNTS = 9;
    public static final int READ_WRITE_CONTACTS_REQUEST = 10;
    public static final int ACCESS_FINE_LOCATION = 11;
    public static final int RECEIVE_SMS = 12;
    public static final int READ_SMS_REQUEST = 13;
    public static final int READ_PHONE_STATE = 14;

    public static DbHelper dbHelper;

    public static DbHelper getDbHelper(Context context){
        if(dbHelper == null){
            dbHelper = new DbHelper(context);
        }

        return dbHelper;
    }


}

//https://maps.googleapis.com/maps/api/directions/json?origin=24.3749376,88.5839582&destination=24.380560700000004,88.5863256&sensor=false&mode=driving&alternatives=true&key=AIzaSyCqK5-KaTX62_RAEz_tszqT7-GLoWpT-VA
