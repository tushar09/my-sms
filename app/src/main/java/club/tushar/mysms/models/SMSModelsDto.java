package club.tushar.mysms.models;

import android.graphics.drawable.Drawable;

public class SMSModelsDto{

    private String id;
    private String threadId;
    private String header;
    private String body;
    private long date;
    private String name;

    private String image;

    private byte type;
    private byte read;

    private int repeatCount = 1;

    private boolean supportReply = false;

    public String getThreadId() {
        return threadId;
    }

    public void setThreadId(String threadId) {
        this.threadId = threadId;
    }

    public boolean isSupportReply() {
        return supportReply;
    }

    public void setSupportReply(boolean supportReply) {
        this.supportReply = supportReply;
    }

    public String getHeader(){
        return header;
    }

    public void setHeader(String header){
        this.header = header;
    }

    public String getBody(){
        return body;
    }

    public void setBody(String body){
        this.body = body;
    }

    public long getDate() {
        return date;
    }

    public void setDate(long date) {
        this.date = date;
    }

    public byte getType(){
        return type;
    }

    public void setType(byte type){
        this.type = type;
    }

    public String getId(){
        return id;
    }

    public void setId(String id){
        this.id = id;
    }

    public byte getRead(){
        return read;
    }

    public void setRead(byte read){
        this.read = read;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public int getRepeatCount(){
        return repeatCount;
    }

    public void setRepeatCount(int repeatCount){
        this.repeatCount = repeatCount;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString(){
        return "SMSModelsDto{" +
                "id='" + id + '\'' +
                ", header='" + header + '\'' +
                ", body='" + body + '\'' +
                ", date='" + date + '\'' +
                ", image=" + image +
                ", type=" + type +
                ", read=" + read +
                '}';
    }
}
