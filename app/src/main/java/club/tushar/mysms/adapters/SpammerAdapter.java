package club.tushar.mysms.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.PopupMenu;
import androidx.appcompat.widget.Toolbar;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.List;

import club.tushar.mysms.R;
import club.tushar.mysms.activity.SpamBoxActivity;
import club.tushar.mysms.activity.SpammerActivity;
import club.tushar.mysms.databinding.RowSmsBinding;
import club.tushar.mysms.db.Spammer;
import club.tushar.mysms.utils.Constants;

public class SpammerAdapter extends RecyclerView.Adapter {

    private Context context;
    private List<Spammer> spammers;

    public SpammerAdapter(Context context, List<Spammer> spammers) {
        this.context = context;
        this.spammers = spammers;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RowSmsBinding binding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.row_sms, parent, false);
        return new Holder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        Holder h = (Holder) holder;
        h.binding.tvBody.setText(spammers.get(position).getNumber());
        Glide.with(context)
                .load(spammers.get(position).getImage())
                .apply(new RequestOptions().placeholder(R.drawable.holder_one))
                .into(h.binding.imageView);
        //holder.binding.imageView.setImageDrawable(dto.get(position).getImage());
        h.binding.tvHeader.setText(spammers.get(position).getName());
        h.binding.tvCount.setVisibility(View.GONE);

        h.binding.getRoot().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                context.startActivity(new Intent(context, SpamBoxActivity.class)
                        .putExtra("id", spammers.get(position).getId())
                        .putExtra("name", spammers.get(position).getName())
                );
            }
        });

        h.binding.getRoot().setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                PopupMenu menu = new PopupMenu(context, view);
                menu.getMenu().add("Remove from spam");
                menu.show();

                menu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem menuItem) {
                        String item = menuItem.getTitle().toString();
                        if(item.equals("Remove from spam")){
                            Constants.getDbHelper(context).removeSpammer(spammers.get(position).getId());
                            spammers.remove(position);
                            notifyDataSetChanged();
                            Toast.makeText(context, "Spammer is removed from spam list", Toast.LENGTH_SHORT).show();
                        }
                        return false;
                    }
                });
                return true;
            }
        });
    }

    @Override
    public int getItemCount() {
        return spammers.size();
    }

    public class Holder extends RecyclerView.ViewHolder {
        RowSmsBinding binding;
        public Holder(@NonNull RowSmsBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}
