package club.tushar.mysms.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.List;

import club.tushar.mysms.R;
import club.tushar.mysms.databinding.RowContactsBinding;
import club.tushar.mysms.models.Contacts;

public class ChooseContactsAdapter extends RecyclerView.Adapter {

    private List<Contacts> contacts;
    private Context context;

    public ChooseContactsAdapter(Context context, List<Contacts> contacts) {
        this.contacts = contacts;
        this.context = context;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RowContactsBinding binding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.row_contacts, parent, false);
        return new ContactsHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder holder, final int position) {
        ContactsHolder contactsHolder = (ContactsHolder) holder;
        contactsHolder.binding.tvName.setText(contacts.get(position).getName());
        contactsHolder.binding.tvNumber.setText(contacts.get(position).getNumber());
        Glide.with(context).load(contacts.get(position).getImage_uri()).apply(new RequestOptions().placeholder(R.drawable.holder_one)).into(contactsHolder.binding.ivImage);


        contactsHolder.binding.getRoot().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent returnIntent = new Intent();
                returnIntent.putExtra("name", contacts.get(position).getName());
                returnIntent.putExtra("number", contacts.get(position).getNumber());
                ((Activity) context).setResult(Activity.RESULT_OK, returnIntent);
                ((Activity) context).finish();
            }
        });
    }

    @Override
    public int getItemCount() {
        return contacts.size();
    }

    private class ContactsHolder extends RecyclerView.ViewHolder {

        RowContactsBinding binding;

        public ContactsHolder(@NonNull RowContactsBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}
