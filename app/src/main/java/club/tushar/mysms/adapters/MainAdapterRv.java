package club.tushar.mysms.adapters;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.AssetManager;
import android.graphics.Typeface;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.PopupMenu;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import club.tushar.mysms.R;
import club.tushar.mysms.activity.MessageDetailsActivity;
import club.tushar.mysms.databinding.RowSmsBinding;
import club.tushar.mysms.db.DbHelper;
import club.tushar.mysms.db.Spammer;
import club.tushar.mysms.models.SMSModelsDto;
import club.tushar.mysms.utils.Constants;

public class MainAdapterRv extends RecyclerView.Adapter<MainAdapterRv.Holder> {

    private final Typeface typefaceNormal;
    private final Typeface typefaceBold;
    private Context context;
    private List<SMSModelsDto> dto;


    public MainAdapterRv(Context context, List<SMSModelsDto> dto) {
        this.context = context;
        this.dto = dto;
        AssetManager am = context.getApplicationContext().getAssets();

        typefaceNormal = Typeface.createFromAsset(am,
                String.format(Locale.US, "fonts/%s", "ProductSansRegular.ttf"));
        typefaceBold = Typeface.createFromAsset(am,
                String.format(Locale.US, "fonts/%s", "ProductSansBold.ttf"));

    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RowSmsBinding binding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.row_sms, parent, false);
        return new Holder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, int position) {
        holder.binding.tvBody.setText(dto.get(position).getBody());
        Glide.with(context)
                .load(dto.get(position).getImage())
                .apply(new RequestOptions().placeholder(R.drawable.holder_one))
                .into(holder.binding.imageView);
        //holder.binding.imageView.setImageDrawable(dto.get(position).getImage());
        holder.binding.tvHeader.setText(dto.get(position).getName());
        holder.binding.tvCount.setText(convertReadableDate(new Date(dto.get(position).getDate())));
        //Log.e("read", dto.get(position).getRead() + "");
        if (dto.get(position).getRead() == 0) {
            holder.binding.tvBody.setTypeface(typefaceBold);
            holder.binding.tvHeader.setTypeface(typefaceBold);
            holder.binding.tvCount.setTypeface(typefaceBold);
        } else {
            holder.binding.tvBody.setTypeface(typefaceNormal);
            holder.binding.tvHeader.setTypeface(typefaceNormal);
            holder.binding.tvCount.setTypeface(typefaceNormal);
        }

        holder.binding.getRoot().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                context.startActivity(new Intent(context, MessageDetailsActivity.class)
                        .putExtra("header", dto.get(position).getHeader())
                        .putExtra("body", dto.get(position).getBody())
                        .putExtra("reply", dto.get(position).isSupportReply())
                        .putExtra("name", dto.get(position).getName())
                );
            }
        });

        holder.binding.getRoot().setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                PopupMenu menu = new PopupMenu(context, view);
                menu.getMenu().add("Add to spam");
                menu.getMenu().add("Delete Conversation");
                menu.show();

                menu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem menuItem) {
                        String item = menuItem.getTitle().toString();
                        if(item.equals("Delete Conversation")){
                            deleteSMSConversation(dto.get(position).getThreadId(), position);
                        }else if(item.equals("Add to spam")){
                            addToSpam(dto.get(position));
                        }
                        return false;
                    }
                });
                return true;
            }
        });
    }

    @Override
    public int getItemCount() {
        return dto.size();
    }

    @Override
    public long getItemId(int position) {
        return dto.get(position).hashCode();
    }

    public class Holder extends RecyclerView.ViewHolder {
        RowSmsBinding binding;
        public Holder(@NonNull RowSmsBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

    private void addToSpam(SMSModelsDto smsModelsDto) {
        new AlertDialog.Builder(context)
                .setTitle("Add to spam")
                .setMessage("Are you sure you want to add this number to spam box?")

                // Specifying a listener allows you to take an action before dismissing the dialog.
                // The dialog is automatically dismissed when a dialog button is clicked.
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Spammer spammer = new Spammer();
                        spammer.setName(smsModelsDto.getName());
                        spammer.setImage(smsModelsDto.getImage());
                        spammer.setNumber(smsModelsDto.getHeader());
                        spammer.setCreated_at(System.currentTimeMillis());
                        spammer.setUpdated_at(System.currentTimeMillis());

                        long affected = Constants.getDbHelper(context).addSpammer(spammer);
                        if(affected != 0){
                            Toast.makeText(context, "Successfully added to spam box", Toast.LENGTH_SHORT).show();
                            Log.e("size", "" + Constants.getDbHelper(context).getSpammers().size());
                        }
                    }
                })

                // A null listener allows the button to dismiss the dialog and take no further action.
                .setNegativeButton(android.R.string.no, null)
                .setIcon(R.mipmap.ic_launcher)
                .show();

    }

    private void deleteSMSConversation(String threadId, int position){
        new AlertDialog.Builder(context)
                .setTitle("Delete Conversation")
                .setMessage("Are you sure you want to delete this Conversation?")

                // Specifying a listener allows you to take an action before dismissing the dialog.
                // The dialog is automatically dismissed when a dialog button is clicked.
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Uri thread = Uri.parse( "content://sms");
                        int deleted = context.getContentResolver().delete( thread, "thread_id=?", new String[]{threadId} );
                        if(deleted != 0){
                            dto.remove(position);
                            notifyDataSetChanged();
                            Toast.makeText(context, "Successfully deleted the conversation", Toast.LENGTH_SHORT).show();
                        }
                    }
                })

                // A null listener allows the button to dismiss the dialog and take no further action.
                .setNegativeButton(android.R.string.no, null)
                .setIcon(R.mipmap.ic_launcher)
                .show();
    };

    private String convertReadableDate(Date date){
        SimpleDateFormat sdf = new SimpleDateFormat("dd EEE");
        String dateText = sdf.format(date);
        return dateText;
    }
}
