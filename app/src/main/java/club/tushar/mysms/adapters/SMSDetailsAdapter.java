package club.tushar.mysms.adapters;

import android.app.Activity;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;

import androidx.appcompat.app.AlertDialog;
import androidx.databinding.DataBindingUtil;
import android.net.Uri;
import androidx.appcompat.widget.PopupMenu;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Toast;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

import club.tushar.mysms.R;
import club.tushar.mysms.activity.ContactsChooserActivity;
import club.tushar.mysms.databinding.RowSmsDetailsBinding;
import club.tushar.mysms.models.SMSDetailsModel;

public class SMSDetailsAdapter extends BaseAdapter {

    private Context context;
    private List<SMSDetailsModel> dto;

    public SMSDetailsAdapter(Context context, List<SMSDetailsModel> dto) {
        this.context = context;
        this.dto = dto;
    }

    @Override
    public int getCount() {
        return dto.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        Holder holder;

        if(convertView == null){
            holder = new Holder(context);
            convertView = holder.binding.getRoot();
            convertView.setTag(holder);
        }else {
            holder = (Holder) convertView.getTag();
        }

        if(dto.get(position).getType() == 1){
            holder.binding.llIncomingHolder.setVisibility(View.VISIBLE);
            holder.binding.tvBodySent.setVisibility(View.GONE);

            holder.binding.tvBodyIncoming.setText(dto.get(position).getBody());

        }else {
            holder.binding.llIncomingHolder.setVisibility(View.GONE);
            holder.binding.tvBodySent.setVisibility(View.VISIBLE);

            holder.binding.tvBodySent.setText(dto.get(position).getBody());
        }

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopupMenu menu = new PopupMenu(context, v);

                String [] words = dto.get(position).getBody().replaceAll("\n", " ").split(" ");
                for(int t = 0; t < words.length; t++){
                    if(words[t].matches(".*\\d+.*") || words[t].contains("http:") || words[t].contains("https:")){
                        //checkUrl(words[t]);
                        menu.getMenu().add(words[t]);
                        Log.e("code", words[t]);
                    }
                    if(isValidEmailAddress(words[t])){
                        menu.getMenu().add(words[t]);
                    }
                }

                menu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem menuItem) {
                        String item = menuItem.getTitle().toString();
                        Log.e("title", item);
                        if(isValidEmailAddress(item)){
                            Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                                    "mailto",item, null));
                            context.startActivity(Intent.createChooser(emailIntent, "Send email..."));
                        }else if(checkUrl(item)){
                            Intent i = new Intent(Intent.ACTION_VIEW);
                            i.setData(Uri.parse(item));
                            context.startActivity(i);
                        }else {
                            Intent intent = new Intent(Intent.ACTION_DIAL);
                            intent.setData(Uri.parse("tel:" + Uri.encode(item)));
                            context.startActivity(intent);
                        }
                        return false;
                    }
                });

                menu.show();
            }
        });

        convertView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                PopupMenu menu = new PopupMenu(context, view);
                menu.getMenu().add("Copy");
                menu.getMenu().add("Share");
                menu.getMenu().add("Delete this SMS");
                menu.show();

                menu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem menuItem) {
                        String item = menuItem.getTitle().toString();
                        if(item.equals("Delete this SMS")){
                            deleteSMS(dto.get(position).getId(), dto.get(position).getThread_id(), position);
                        }else if(item.equals("Copy")){
                            ClipboardManager clipboard = (ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
                            ClipData clip = ClipData.newPlainText(dto.get(position).getThread_id(), dto.get(position).getBody());
                            clipboard.setPrimaryClip(clip);
                            Toast.makeText(context, "Copied", Toast.LENGTH_SHORT).show();
                        }else if(item.equals("Share")){
                            Intent sendIntent = new Intent();
                            sendIntent.setAction(Intent.ACTION_SEND);
                            sendIntent.putExtra(Intent.EXTRA_TEXT, dto.get(position).getBody());
                            sendIntent.setType("text/plain");

                            Intent shareIntent = Intent.createChooser(sendIntent, null);
                            context.startActivity(shareIntent);
                        }else if(item.equals("Forward")){
                            ((Activity)context).startActivityForResult(new Intent(context, ContactsChooserActivity.class), 10);
                        }
                        return false;
                    }
                });
                return true;
            }
        });

        return convertView;
    }

    private void deleteSMS(String id, String thread_id, int position) {
        new AlertDialog.Builder(context)
                .setTitle("Delete SMS")
                .setMessage("Are you sure you want to delete this SMS?")

                // Specifying a listener allows you to take an action before dismissing the dialog.
                // The dialog is automatically dismissed when a dialog button is clicked.
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Uri thread = Uri.parse( "content://sms");
                        int deleted = context.getContentResolver().delete( thread, "thread_id=? and _id=?", new String[]{String.valueOf(thread_id), String.valueOf(id)} );
                        if(deleted != 0){
                            dto.remove(position);
                            notifyDataSetChanged();
                            Toast.makeText(context, "Successfully deleted the SMS", Toast.LENGTH_SHORT).show();
                        }
                    }
                })

                // A null listener allows the button to dismiss the dialog and take no further action.
                .setNegativeButton(android.R.string.no, null)
                .setIcon(R.mipmap.ic_launcher)
                .show();
    }

    private boolean checkUrl(String inputUrl){
        URL url = null;
        try {
            url = new URL(inputUrl);
        } catch (MalformedURLException e) {
            Log.v("myApp", "bad url entered");
        }
        if (url == null)
            return false;
        else
            return true;
    }

    private boolean isValidEmailAddress(String email) {
        String ePattern = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$";
        java.util.regex.Pattern p = java.util.regex.Pattern.compile(ePattern);
        java.util.regex.Matcher m = p.matcher(email);
        return m.matches();
    }

    private class Holder{
        RowSmsDetailsBinding binding;

        public Holder(Context context){
            binding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.row_sms_details, null, true);
        }
    }
}
