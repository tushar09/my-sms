package club.tushar.mysms.adapters;

import android.content.Context;
import android.content.Intent;
import androidx.databinding.DataBindingUtil;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import club.tushar.mysms.R;
import club.tushar.mysms.activity.MessageDetailsActivity;
import club.tushar.mysms.databinding.RowSmsBinding;
import club.tushar.mysms.models.SMSModelsDto;

public class MainAdapter extends BaseAdapter {

    private Context context;
    private List<SMSModelsDto> dto;

    public MainAdapter(Context context, List<SMSModelsDto> dto) {
        this.context = context;
        this.dto = dto;
    }

    @Override
    public int getCount() {
        return dto.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        Holder holder;

        if (convertView == null) {
            holder = new Holder(context);
            convertView = holder.binding.getRoot();
            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }

        holder.binding.tvBody.setText(dto.get(position).getBody());
        //holder.binding.imageView.setImageDrawable(dto.get(position).getImage());
        holder.binding.tvHeader.setText(dto.get(position).getName());
        holder.binding.tvCount.setText(convertReadableDate(new Date(dto.get(position).getDate())));
        //Log.e("read", dto.get(position).getRead() + "");
        if (dto.get(position).getRead() == 0) {
            holder.binding.tvBody.setTypeface(null, Typeface.BOLD);
            holder.binding.tvHeader.setTypeface(null, Typeface.BOLD);
            holder.binding.tvCount.setTypeface(null, Typeface.BOLD);
        } else {
            holder.binding.tvBody.setTypeface(null, Typeface.NORMAL);
            holder.binding.tvHeader.setTypeface(null, Typeface.NORMAL);
            holder.binding.tvCount.setTypeface(null, Typeface.NORMAL);
        }

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                context.startActivity(new Intent(context, MessageDetailsActivity.class)
                        .putExtra("header", dto.get(position).getHeader())
                        .putExtra("body", dto.get(position).getBody())
                        .putExtra("reply", dto.get(position).isSupportReply())
                        .putExtra("name", dto.get(position).getName())
                );
            }
        });

        return convertView;
    }

    private String convertReadableDate(Date date){
        SimpleDateFormat sdf = new SimpleDateFormat("dd EEE");
        String dateText = sdf.format(date);
        return dateText;
    }

    private class Holder {
        RowSmsBinding binding;

        public Holder(Context context) {
            binding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.row_sms, null, true);
        }
    }
}
