package club.tushar.mysms.db;

import android.content.Context;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.List;

public class DbHelper {
    private final String DB_NAME = "spam-list-db";

    private final DaoSession daoSession;
    private DaoMaster daoMaster;

    private SpammerDao spammerDao;
    private SpamBoxDao spamBoxDao;

    private SQLiteOpenHelper sqLiteOpenHelper;

    public DbHelper(Context context){
        sqLiteOpenHelper = new DaoMaster.DevOpenHelper(context, DB_NAME, null);
        daoMaster = new DaoMaster(sqLiteOpenHelper.getWritableDatabase());
        daoSession = daoMaster.newSession();

        spammerDao = daoSession.getSpammerDao();
        spamBoxDao = daoSession.getSpamBoxDao();
    }

    public List<Spammer> getSpammers(){
        return spammerDao.queryBuilder().orderDesc(SpammerDao.Properties.Id).build().list();
    }

    public Spammer getSpammerByNumber(String number){
        return spammerDao.queryBuilder().where(SpammerDao.Properties.Number.eq(number)).unique();
    }

    public long addSpammer(Spammer spammer){
        return spammerDao.insertOrReplace(spammer);
    }

    public void removeSpammer(long spammerId){
        spammerDao.deleteByKey(spammerId);
    }

    public long addToSmapBox(SpamBox spamBox){
        return spamBoxDao.insert(spamBox);
    }

    public List<SpamBox> getSpamBoxBySpammerId(long spammerId){
        return spamBoxDao.queryBuilder().orderDesc(SpamBoxDao.Properties.Created_at).list();
    }
}
