package club.tushar.mysms.db;

import org.greenrobot.greendao.annotation.*;

// THIS CODE IS GENERATED BY greenDAO, EDIT ONLY INSIDE THE "KEEP"-SECTIONS

// KEEP INCLUDES - put your custom includes here
// KEEP INCLUDES END

/**
 * Entity mapped to table "SPAM_BOX".
 */
@Entity
public class SpamBox {

    @Id(autoincrement = true)
    private Long id;
    private Long spammer_id;
    private String body;
    private Long date;
    private Long created_at;
    private Long updated_at;

    // KEEP FIELDS - put your custom fields here
    // KEEP FIELDS END

    @Generated
    public SpamBox() {
    }

    public SpamBox(Long id) {
        this.id = id;
    }

    @Generated
    public SpamBox(Long id, Long spammer_id, String body, Long date, Long created_at, Long updated_at) {
        this.id = id;
        this.spammer_id = spammer_id;
        this.body = body;
        this.date = date;
        this.created_at = created_at;
        this.updated_at = updated_at;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getSpammer_id() {
        return spammer_id;
    }

    public void setSpammer_id(Long spammer_id) {
        this.spammer_id = spammer_id;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public Long getDate() {
        return date;
    }

    public void setDate(Long date) {
        this.date = date;
    }

    public Long getCreated_at() {
        return created_at;
    }

    public void setCreated_at(Long created_at) {
        this.created_at = created_at;
    }

    public Long getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(Long updated_at) {
        this.updated_at = updated_at;
    }

    // KEEP METHODS - put your custom methods here
    // KEEP METHODS END

}
